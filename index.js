const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => res.send('Welcome to GitLab CI/CD with GitLab with Automation Team'))

app.listen(port, () => console.log(`Example app listening at ${port}`))